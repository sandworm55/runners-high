//
//  backgroundParent.swift
//  Runners High
//
//  Created by Stewart Holmes on 2017-07-05.
//  Copyright © 2017 Jordan Moore. All rights reserved.
//

import SpriteKit

class BackgroundParent : SKSpriteNode, EventListenerNode {
   
    var speedInPixels = CGFloat(0.0)
    var pausedSpeed = false
    
    var bgChildren : [BackgroundController] = []
    
    func didMoveToScene() {
        
        enumerateChildNodes(withName: "//bg*", using: { node, _ in
           if let BGC = node as? BackgroundController {
             self.bgChildren.append(BGC)
            BGC.didMoveToScene()
            }
        })
    }
    
    func updatePosition() {
        for i in bgChildren { i.updatePosition() }
    }
    
    func pauseBG(){
        for i in bgChildren { i.pauseBG() }
    }
    
    func unpauseBG() {
        for i in bgChildren { i.unpauseBG() }
    }
}
