import SpriteKit

class MainMenu: SKScene {
    
    let settings = UserDefaults.standard
    let zero = 0 as Float
    // arrived at scene
    override func didMove(to view: SKView) {
        // running all childrens setup
        enumerateChildNodes(withName: "//*", using: { node, _ in
            if let eventListenerNode = node as? EventListenerNode {
                eventListenerNode.didMoveToScene()
            }
        })
        
        if true{
            print("sfx volume -> ",settings.float(forKey: OptionsPane.SFX_VOLUME))
            print("music volume -> ",settings.float(forKey: OptionsPane.MUSIC_VOLUME))
            print("sfx toggle -> ",settings.bool(forKey: OptionsPane.SFX_TOGGLE))
            print("music toggle -> ",settings.bool(forKey: OptionsPane.MUSIC_TOGGLE))
        }
        
        // initilizing sfx volume from settings
        SKTAudio.sharedInstance().soundEffectPlayer?.volume = settings.bool(forKey: OptionsPane.SFX_TOGGLE) ? settings.float(forKey: OptionsPane.SFX_VOLUME) : zero
        print(SKTAudio.sharedInstance().soundEffectPlayer?.volume as Any)
        // initilizing bgm volume from settings
        SKTAudio.sharedInstance().backgroundMusicPlayer?.volume = settings.bool(forKey: OptionsPane.MUSIC_TOGGLE) ? settings.float(forKey: OptionsPane.MUSIC_VOLUME) : zero
        
        // starting menu bgm
        SKTAudio.sharedInstance().playBackgroundMusic("MainMenuSoundtrack.aac")
    }
    
    // scene gets a touch
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        if let touch = touches.first {
            
            let pos = touch.location(in: self) // get touch location
            let node = self.atPoint(pos) // get node that was touched
            
            if let button = node as? InteractiveNode {
                button.interact()
                SKTAudio.sharedInstance().playSoundEffect("MenuClick.aac") // click sound
            } else if let bar = node as? DragNode {
                bar.dragEnd(pos: pos)
            } else {
                //print("can't do anything with that")
            }
        }
    }
    
    // same as above, but for drags
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        if let touch = touches.first {
            
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            
            if let bar = node as? DragNode {
                bar.dragging(pos: pos)
            } else {
                //print("can't drag that anything with that")
            }
        }
    }
}
