//
//  CheckBox.swift
//  Runners High
//
//  Created by Eugene Kahiya on 2017-06-23.
//  Copyright © 2017 Jordan Moore. All rights reserved.
//

import SpriteKit


class CheckBox: SKSpriteNode, InteractiveNode, EventListenerNode {
   
    let unchecked = SKTexture(imageNamed: "check_box") // texture for unchecked checkbox
    let checked = SKTexture(imageNamed: "check_box_checked") // texture for checked checkbox
    
    var value = true // value of this object
    let settings = UserDefaults.standard // shortcut to the settings values
    
    func didMoveToScene() {
        value = settings.bool(forKey: name!)
        setTexture()
    }
    
    // when this is clicked
    func interact() {
        print(name!)
        value = !value // switch the value to the opposite
        setTexture()
        
        //  change the status of the volume for the appropriate sound object, sfx or bgm, to 0, or the current volume
        if name! == OptionsPane.MUSIC_TOGGLE {
            SKTAudio.sharedInstance().backgroundMusicPlayer?.volume = value ? settings.float(forKey: OptionsPane.MUSIC_VOLUME) : 0
        } else if name! == OptionsPane.SFX_TOGGLE {
            SKTAudio.sharedInstance().soundEffectPlayer?.volume = value ? settings.float(forKey: OptionsPane.SFX_VOLUME) : 0
        }
        // set the new value to the settings for the toggle
        settings.set(value, forKey: name!)
        print("audio")
        print("BGM -> ", SKTAudio.sharedInstance().backgroundMusicPlayer?.volume ?? "err - no bgm volume")
        print("SFX -> ", SKTAudio.sharedInstance().soundEffectPlayer?.volume ?? "err - no sfx volume")
        
        print("settings")
        print(" -> ", settings.float(forKey: name!))
        print(" -> ", settings.bool(forKey: name!))
        

    }
    
    func setTexture() {
        texture = value ? checked : unchecked
    }
}
