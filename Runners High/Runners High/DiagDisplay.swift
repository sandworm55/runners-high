//
//  DiagDisplay.swift
//  Runners High
//
//  Created by Sean Dowd-Taylor on 2017-07-06.
//  Copyright © 2017 Jordan Moore. All rights reserved.
//

import SpriteKit

class DiagDisplay : SKLabelNode {
    
    //display to screen
    var shown = false
    
    //array of dialogue lines
    var quips: [String] = ["Let's start with a little jog to warm up the glutes.", "Whew, I shouldn't have done hot yoga with a belly full of milk.", "Off to a running start...", "I never run from my fears (except maybe spiders).", "At times like these I wish my nipples had flashlights.", "I put the LEG in LEGolas (forest, elves, get it?)", "I think I ran, I think I ran...", "This reminds me of the time I ran to the top of Mount Everest.", "Run for the hills, run for your liiiiiiiife!", "I’ll be running across this mountain, here I go!", "Life’s a beach and then you fry (in the sun...get it?)", "Global warming feels pretty nice, maybe Trump is onto something…", "Mr. Sun, Sun. Mr. Golden Sun, please stop burning me.", "I’m running out of time!", "And I ruuuun, I run so far awaaay...", "I can feel it in my soles!", "I never run from my problems...nor irony! A ha!", "Try running a mile in my shoes, or should I say 5, 097, 825 miles.", "Short breaths as I go, forget everything I know. Become one with the path, I know I’ve felt this before!", "I got sole but I’m not a soldier!", "Whew, I think I’m finally breaking a sweat."]
    
    
    func changeQuip() {
        let index = Int(arc4random_uniform(21))
        let changeQuip = quips[index]
        
        text = changeQuip
    }
    
    
    //randomizes the line taken from array
   //var randomQuip = quips[arc4random() % 21]
    
//    func diagDisplay() {
//    
//    //label configures
//    let dialogue = SKLabelNode(fontNamed: "Optima-Bold")
//    dialogue.text = String(randomQuip)
//    dialogue.fontSize = 15
//    dialogue.fontColor  = SKColor.white
//    dialogue.verticalAlignmentMode = .center
//    //dialogue.position = CGPoint()
//    
//    
//    addChild(dialogue)
//    }
    
}
