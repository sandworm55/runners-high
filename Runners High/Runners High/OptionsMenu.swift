//
//  OptionsMenu.swift
//  Runners High
//
//  Created by Eugene Kahiya on 2017-06-26.
//  Copyright © 2017 Jordan Moore. All rights reserved.
//

import SpriteKit


class OptionsMenu : SKSpriteNode, EventListenerNode {
    
    func didMoveToScene() {       

    }

    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        if let touch = touches.first {
            
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            print(node.name!)
            let button = childNode(withName: node.name!) as! SKSpriteNode
            
            button.touchesEnded(touches, with: event)
        }
    }
}
