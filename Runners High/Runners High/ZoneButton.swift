//
//  ZoneButton.swift
//  Runners High
//
//  Created by Jordan Moore on 2017-06-22.
//  Copyright © 2017 Jordan Moore. All rights reserved.
//

import SpriteKit

class ZoneButton : SKSpriteNode , EventListenerNode , InteractiveNode {
    
    var level = 0 // the level this button represents
    var active = false // weather the player has unlocked this level
    
    let activeImage = SKTexture(imageNamed: "map_node_active") // the texture to use should the player have unlocked this zone
    let settings = UserDefaults.standard
    
    let names : [String] = [ "park", "forest","mountain", "beach", "cave", "lightjungle", "desert" ]
    static let LEVEL_COMPLETED = "levelCompleted"
    static let THIS_LEVEL = "thisLevel"
    
    
    func interact() {
        // switch comment to other if statment to not check if the level has been completed
         if true {
        //if active {
            scene?.view?.presentScene(level(level)) // switch to the level associated to this button
        }
    }
    
    // takes an int and returns a new game scene with the current level set
    func level(_ levelNum: Int) -> GameScene? {
        // let scene = GameScene(fileNamed: "Level\(levelNum)")!
        let scene = SKScene(fileNamed: "GameScene") as! GameScene
        scene.currentLevel = levelNum
        scene.scaleMode = .aspectFill
        scene.levelType = names[levelNum - 1]
        return scene
    }
    
    func didMoveToScene() {
        level = userData?.value(forKey: ZoneButton.THIS_LEVEL) as! Int // setting this level to the value in the User Data pane
        
        active = haveWeUnlockedThis() // setting active if we have the last level beaten
        
        if active { texture = activeImage } // if it is active then change the image
    }
    
    // checks if the last level has been beaten, and returns Bool
    func haveWeUnlockedThis() -> Bool {
        let thisLevel = userData?.value(forKey: ZoneButton.THIS_LEVEL) as! Int
        return (thisLevel - 1) <= settings.integer(forKey: ZoneButton.LEVEL_COMPLETED)
    }
}
