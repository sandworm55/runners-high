//
//  OptionsBack.swift
//  Runners High
//
//  Created by Eugene Kahiya on 2017-06-27.
//  Copyright © 2017 Jordan Moore. All rights reserved.
//

import SpriteKit

class OptionsBack : SKSpriteNode, InteractiveNode {
    
    func interact() {
        let thisParent = parent as! OptionsPane
        thisParent.hide()
    }
}
