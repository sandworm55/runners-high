//
//  OptionsButton.swift
//  Runners High
//
//  Created by Eugene Kahiya on 2017-06-26.
//  Copyright © 2017 Jordan Moore. All rights reserved.
//

import SpriteKit

class OptionsButton: SKSpriteNode , InteractiveNode {
    
    func interact() {
        let optionsPane = parent?.childNode(withName: "//optionsPane") as! OptionsPane
        optionsPane.show()
    }
}
