//
//  BackgoundController.swift
//  Runners High
//
//  Created by Jordan Moore on 2017-06-30.
//  Copyright © 2017 Jordan Moore. All rights reserved.
//

import SpriteKit

class BackgroundController : SKSpriteNode, EventListenerNode {
    var speedInPixels = CGFloat(0.0)
    var pausedSpeed = false
    var sizeOfScreen : CGFloat!
    
    func didMoveToScene() {
        if let temp = userData?.object(forKey: "speedInPixels") as? Float{
            speedInPixels = CGFloat(temp)
        } else {
            speedInPixels = 0
        }
        let thisParent = parent as! BackgroundParent
        sizeOfScreen = thisParent.size.width
    }
    
    func updatePosition() {
        if !pausedSpeed {
            position.x = position.x + -speedInPixels
            if position.x <= -sizeOfScreen {
                position.x = sizeOfScreen + (position.x + sizeOfScreen)
            }
        }
    }
        
    func pauseBG() { pausedSpeed = true }
    func unpauseBG() { pausedSpeed = false }
}
