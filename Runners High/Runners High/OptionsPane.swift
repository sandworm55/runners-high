//
//  Options.swift
//  Runners High
//
//  Created by Eugene Kahiya on 2017-06-26.
//  Copyright © 2017 Jordan Moore. All rights reserved.
//

import SpriteKit


class OptionsPane: SKSpriteNode {
    
    var shown = false
    
    static let MUSIC_TOGGLE = "musicToggle"
    static let SFX_TOGGLE = "sfxToggle"
    
    static let MUSIC_VOLUME = "musicVolume"
    static let SFX_VOLUME = "sfxVolume"
    
    func show() {
        if !shown {
        self.run( SKAction.moveBy(x: 600 ,y: 0, duration: 1))
            shown = true
        }
       SKTAudio.sharedInstance().playSoundEffect("OptionSwipe.aac")
    }
    
    func  hide(){
        if shown {
            self.run( SKAction.moveBy(x: -600 ,y: 0, duration: 1))
            shown = false
        }
        SKTAudio.sharedInstance().playSoundEffect("OptionBackSwipe.aac")
    }
}
