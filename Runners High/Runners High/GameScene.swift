import SpriteKit
import UIKit
import GameplayKit

class GameScene: SKScene {
    
    private var planning = false
    private var moving = false
    private var obstaclesSet = false
	private var obstaclesMovedIn = false
    private var commandList: [Int] = []
    private var obstaclesList: [Int] = []
    private var obstacleSprites: [SKSpriteNode] = []
    private var arrowSprites: [SKSpriteNode] = []
    private var playerActions: [SKAction] = []
    private var timeLeft = TimeInterval(4)
    private var currentTimeElapsed = TimeInterval(0)
    private var lastUpdateTime:TimeInterval = 0
    private var dt:TimeInterval = 0
    private var timerRunning = false
    
    let commandLimit = 5 // obstacle amount
    let screenSpeed = 3 // duration of screen movement
    let heartEmpty = SKTexture(imageNamed: "heart_empty")
    let arrow = SKTexture(imageNamed: "movebox")
    let moveLeft = SKAction.moveBy(x: -2048, y: 0, duration: 3)
	
    var timePerMove : Double!
    var playerJump : SKAction!
    var playerSlide : SKAction!
    var playerRun : SKAction!
	var playerCrash : SKAction!
    
    var playerHealth = 3
    var playerAlive = true
    
    var background: BackgroundParent!
	var player: SKSpriteNode!
	var timerLabel = SKLabelNode()
    var playerJumpTextures:[SKTexture] = []
    var playerSlideTextures:[SKTexture] = []
    var playerRunTextures:[SKTexture] = []
    var playerCrashTextures:[SKTexture] = []
	var levelType : String!
	var dialogue: DiagDisplay!
	var currentLevel = 0
	var round = 0
	
    override func didMove(to view: SKView) {
		
        timePerMove = Double(screenSpeed) / Double(commandLimit + 1)
		
		let BGNodeSource = "background_\(levelType!)"
		let path = Bundle.main.path(forResource: BGNodeSource, ofType: "sks")
		let BGNode = SKReferenceNode (url: NSURL (fileURLWithPath: path!) as URL)
		addChild(BGNode)
		
		SKTAudio.sharedInstance().playBackgroundMusic("Level\(currentLevel)Soundtrack.aac")
		
        background = childNode(withName: "//bgSky") as! BackgroundParent
		background.zPosition = -10
        player = childNode(withName: "Player") as! SKSpriteNode
		timerLabel = childNode(withName: "timer") as! SKLabelNode
		
        playerRunTextures.removeAll()
        for i in 0...1 {
            playerRunTextures.append(SKTexture(imageNamed: "runner_running_\(i)"))
        }
        
        playerJumpTextures.removeAll()
        for i in 0...4 {
            playerJumpTextures.append(SKTexture(imageNamed: "runner_jump_\(i)"))
        }
        for i in (0...3).reversed() {
            playerJumpTextures.append(SKTexture(imageNamed: "runner_jump_\(i)"))
        }
        
        playerSlideTextures = [SKTexture(imageNamed: "runner_slide")]
		
		playerCrashTextures = [SKTexture(imageNamed: "runner_crash")]
		
        playerCrash = SKAction.group([
			SKAction.animate(
				with: playerCrashTextures,
				timePerFrame: timePerMove / Double(playerCrashTextures.count),
				resize: true,
				restore: true),
			SKAction.run {
				print("boop")
				self.playerHealth -= 1
			}])
		
		playerJump = SKAction.animate(
						with: playerJumpTextures,
						timePerFrame: timePerMove / Double(playerJumpTextures.count),
						resize: true,
						restore: true)
		
        playerSlide = SKAction.animate(with: playerSlideTextures, timePerFrame: timePerMove, resize: true, restore: true)
        
        playerRun = SKAction.animate(with: playerRunTextures, timePerFrame: timePerMove / Double(playerRunTextures.count), resize: true, restore: true)
        
        player.run(SKAction.repeatForever(playerRun))
      
        print("activating")
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        view.addGestureRecognizer(swipeDown)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up
        view.addGestureRecognizer(swipeUp)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.respondToTap))
        view.addGestureRecognizer(tap)
        
        setObstacles()
        
        background.didMoveToScene()
		
		dialogue = DiagDisplay()
		dialogue.isHidden = false
		dialogue.position = CGPoint.zero
		dialogue.horizontalAlignmentMode = .right
		dialogue.verticalAlignmentMode = .center
		dialogue.changeQuip()
		dialogue.color = SKColor.white
		dialogue.fontSize = 35
		dialogue.fontName = "Arial-BoldMT"
        addChild(dialogue)
        print("current Level = " , currentLevel)
    }
	
    
    override func willMove(from view: SKView) {
        let dismissKeyboardTap = UITapGestureRecognizer(target: self, action: #selector(GameScene.dismissKeyboard))
        dismissKeyboardTap.cancelsTouchesInView = false
        view.addGestureRecognizer(dismissKeyboardTap)
    }
    
    func dismissKeyboard() {
        view?.endEditing(true)
    }
    
    func updateTimer(_ currentTime: TimeInterval) {
        if timerRunning {
			dialogue.isHidden = true
            if lastUpdateTime > 0 {
                dt = currentTime - lastUpdateTime
            }
            else{
                dt = 0
            }
            currentTimeElapsed = dt + currentTimeElapsed
			
			let timerCountdown = timeLeft - currentTimeElapsed
			var timerString = String(timerCountdown)
			
			if timerCountdown <= 0 && playerAlive {
				timeLeft = timeLeft - TimeInterval(0.05)
				timerString = "0.00"
				round = round + 1
				dialogue.isHidden = false
				dialogue.changeQuip()
				if round == 5 {
					print("level complete")
					let settings = UserDefaults.standard
					if settings.integer(forKey: ZoneButton.LEVEL_COMPLETED) < currentLevel {
						print("unlocked next level")
						settings.set(currentLevel, forKey: ZoneButton.LEVEL_COMPLETED)
					}
				}
			}
			timerLabel.isHidden = false
			timerLabel.text = timerString.substring(to: timerString.index(timerString.startIndex, offsetBy: 4))
        }
		else
		{
			timerLabel.isHidden = true
		}
        lastUpdateTime = currentTime
    }
    
    func fillCommandList() {
        if commandList.count < commandLimit {
            let fill = commandLimit - commandList.count
            for _ in (fill) ... commandLimit{
                addCommand(command: 0)
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
		updateHearts()
        if playerAlive {
            
            updateTimer(currentTime)
            background.updatePosition()
            
            if currentTimeElapsed >= timeLeft { fillCommandList() }
            
            if obstaclesSet && !planning { useTheCommandList() }
			
            if !obstaclesSet && !moving { setObstacles() }
			
        } else {
            // Guy, you dead
            if !moving {
                let scene = GameScene(fileNamed: "MainMenu")!
                scene.scaleMode = .aspectFill
                view?.presentScene(scene)
            }
        }
    }
    
    // obstaclesSet = true
    // generates random list of obstacles based on commandLimit
    // runs obstacles()
    func setObstacles() {
		
        obstaclesList.removeAll()
		
		while obstaclesSet == false {
			obstaclesList.removeAll()
			for _ in 1...commandLimit {
				let random = Int(arc4random_uniform( currentLevel > 3 ? 3 : 2 ))
				
				obstaclesList.append(random)
				
				if random != 0 {
					obstaclesSet = true
				}
			}
		}
		
		
        print("count -> ",obstaclesList.count, " = " ,obstaclesList)
		
        obstacles()
    }
    
    // clears obstaclesSprites
    // draws obstacles to right of screen to match obstaclesLists[] values
    // moves obstacles left to on screen
    // clears arrowSprites
    // sets planning = true
    func obstacles() {
        
        for obstacle in 0...obstaclesList.count-1 {
			var imageSearch = true
			var i = 1
			
            switch obstaclesList[obstacle] {
            case 0:
                //clear
                break
            case 1:
				while imageSearch {
					if ((UIImage(named:"obstacle_\(levelType!)_jump_\(i)")) != nil) {
						i = i + 1
					} else {
						imageSearch = false
						i = i - 1
					}
					
					print("jump image count : " , i)
				}
				
				let random = Int(arc4random_uniform(UInt32(i)))
                obstacleSprites.append(SKSpriteNode(imageNamed: "obstacle_\(levelType!)_jump_\(random+1)"))
				
				obstacleSprites.last?.anchorPoint = CGPoint(x: 0.5 , y: 0)
				
                obstacleSprites.last?.position = CGPoint(x: (obstacle + 1)  * (2048/(commandLimit+1)) + (2048/(commandLimit+1)/2) + 1024,
                                                         y: 200 - 1536/2)
                
                addChild(obstacleSprites.last!)
                break
            case 2:
					while imageSearch {
						if ((UIImage(named:"obstacle_\(levelType!)_slide_\(i)")) != nil) {
							i = i + 1
						} else {
							imageSearch = false
							i = i - 1
						}
						
						print("slide image count : " , i)
					}
					
					let random = Int(arc4random_uniform(UInt32(i)))
					obstacleSprites.append(SKSpriteNode(imageNamed: "obstacle_\(levelType!)_slide_\(random+1)"))
					
					obstacleSprites.last?.anchorPoint = CGPoint(x: 0.5 , y: 0)
					obstacleSprites.last?.position = CGPoint(x: (obstacle + 1) * (2048/(commandLimit+1)) + (2048/(commandLimit+1)/2) + 1024,
															 y: 200 - 1536/2)
					addChild(obstacleSprites.last!)
                break
            default:
                break
            }
        }
		
        for object in obstacleSprites {
            object.run(moveLeft)
            {
				self.player.speed = 0
                self.background.pauseBG()
				self.timerRunning = true
				self.obstaclesMovedIn = true
            }
        }
		
		obstaclesMovedIn = false
		
        for arrow in arrowSprites {
            arrow.removeFromParent()
        }
		
		self.planning = true
    }
    
    // runs addCommand(1 or 2) for jump or slide on up or down swipe
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.down:
                addCommand(command: 2)
            case UISwipeGestureRecognizerDirection.up:
                addCommand(command: 1)
            default:
                break
            }
        }
    }
    
    // runs addCommand(0) for tap
    func respondToTap(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            addCommand(command: 0)
        }
    }
    
    // if planning == true
    // add new command value to commandList
    // add new arrow to arrowSprites
    // match arrow location and rotation to last command added
    // if commandList.count == commandLimit {planning = false}
    func addCommand(command: Int) {
        if planning && obstaclesMovedIn {
            
            commandList.append(command)
            
            arrowSprites.append(SKSpriteNode(texture: arrow))
            
            arrowSprites.last?.position = CGPoint(x: (commandList.count * 150) + 100 ,
                                                  y: 375)
            switch command {
            case 1:
                arrowSprites.last?.zRotation = CGFloat(90.0).degreesToRadians()
                break
            case 2:
                arrowSprites.last?.zRotation = CGFloat(-90.0).degreesToRadians()
                break
            default:
                arrowSprites.last?.zRotation = CGFloat(0.0).degreesToRadians()
                break
            }
            
            arrowSprites.last?.zPosition = 5
            
            arrowSprites.last?.xScale = 2
            arrowSprites.last?.yScale = 2
            
            addChild(arrowSprites.last!)
        }
        
        if  commandList.count == commandLimit {
            planning = false
        }
    }
    
    // prints commandList
    // moves player with commands
    // move obstacles & player left -> removes obstacle sprites
    // clear playerActions & commandList
    // obstaclesSet = false
    // runs moveSceneLeft()
    func useTheCommandList() {
        
        timerRunning = false
        currentTimeElapsed = 0
        
        self.player.speed = 1
		
        moving = true
        
        ////////////
        playerActions.append(SKAction.wait(forDuration: timePerMove / 2))
        
        print("useTheCommandList() -> ",commandList)
        
        for obstacleNum in 0...commandList.count-1 {
            switch commandList[obstacleNum] {
            case 0:
				
                if obstaclesList[obstacleNum] != 0 {
					playerActions.append(playerCrash)
				} else {
					playerActions.append(playerRun)
				}
                break
            case 1:
                if obstaclesList[obstacleNum] != 1  && obstaclesList[obstacleNum] != 0 {
					playerActions.append(playerCrash)
				} else {
					 playerActions.append(playerJump)
				}
                break
            case 2:
                if obstaclesList[obstacleNum] != 2 && obstaclesList[obstacleNum] != 0 {
					playerActions.append(playerCrash)
				} else {
					playerActions.append(playerSlide)
				}
                break
            default:
                break
            }
        }
        
        playerActions.append(SKAction.wait(forDuration: timePerMove / 2))
        ////////////
        

        let sequence = SKAction.sequence(playerActions)

        let playerMove = SKAction.moveBy(x: 2048, y: 0, duration: TimeInterval(screenSpeed))
        
        let group = SKAction.group([sequence, playerMove])
        
        player.run(group) {
            
            for arrow in self.arrowSprites {
                arrow.removeFromParent()
            }
            
            self.arrowSprites.removeAll()
            
            self.moveSceneLeft()
			
			print("\n")
			
			print("playerHealth -> ", self.playerHealth)
			if self.playerHealth <= 0 {
				self.playerAlive = false
			}
        }
        playerActions.removeAll()
        
        commandList.removeAll()
        
        obstaclesSet = false

    }
	
	func updateHearts(){
		for item in 1 ... 3 {
			let thisHeart = self.childNode(withName: "Heart\(item)") as! SKSpriteNode
			
			if item > self.playerHealth {
				thisHeart.texture = self.heartEmpty
			}
		}
	}
    
    // clears ArrowSprites
    // move backgrounds 1 & 2 left
    // move ojects and player left
    // planning = true
    func moveSceneLeft() {
        background.unpauseBG()
        
        for object in self.obstacleSprites {
            object.run(moveLeft){
                object.removeFromParent()
                
                self.moving = false
            }
        }
        obstacleSprites.removeAll()
        
        self.arrowSprites.removeAll()
        
        player.run(moveLeft){
            self.planning = true
        }
    }
}

protocol EventListenerNode {
    func didMoveToScene()
}

protocol InteractiveNode {
    func interact()
}

protocol DragNode {
    func dragging(pos :  CGPoint)
    func dragEnd(pos :  CGPoint)
}

enum Instructions {
    case jump, slide, run
}
