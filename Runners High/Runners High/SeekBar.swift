//
//  SeekBar.swift
//  Runners High
//
//  Created by Jordan Moore on 2017-06-20.
//  Copyright © 2017 Jordan Moore. All rights reserved.
//

import SpriteKit

class SeekBar: SKSpriteNode, EventListenerNode, DragNode {

    var value = CGFloat(0.0)
    var node : SKSpriteNode?
    
    let defaults = UserDefaults.standard
    
    func didMoveToScene () {
        node = childNode(withName: "Node") as? SKSpriteNode
        valueSet(CGFloat(defaults.float(forKey: name!)))
    }
    
    func dragEnd(pos : CGPoint) {
        dragging(pos: pos)
    }
    
    func dragging(pos: CGPoint) {
        let diff = CGPoint(x: (pos.x - (convert(position, to: (node?.scene)!)).x), y:0)
        node?.position = diff
        
        let top = (diff.x + (size.width / 2))
        value = top / size.width
        
        defaults.set(value, forKey: name!)
    }
    
    func valueSet(_ value : CGFloat){
        self.value = value
        
        node?.position.x = (convert(position, to: (node?.scene)!)).x - ((size.width * CGFloat(value)) - (size.width/2))
    }
}
